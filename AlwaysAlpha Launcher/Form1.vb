﻿Imports System.Environment
Imports System.IO
Imports System.Net

Public Class Form1
    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        My.Settings.username = TextBox1.Text
        My.Settings.Save()
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        My.Settings.username = TextBox1.Text
        My.Settings.Save()
        Dim Stor = GetFolderPath(SpecialFolder.ApplicationData) + "\AlwaysAlpha"
        If My.Computer.FileSystem.DirectoryExists(Stor) = False Then
            My.Computer.FileSystem.CreateDirectory(Stor)
        End If
        Directory.SetCurrentDirectory(Stor)
        If My.Computer.FileSystem.FileExists("minecraft.jar") Then
            Shell("launch.exe " & My.Settings.username)
            Me.Close()
        Else
            If My.Computer.FileSystem.FileExists("install.exe") Then
                My.Computer.FileSystem.DeleteFile("install.exe")
            End If
            Try
                My.Computer.Network.DownloadFile("https://raw.githubusercontent.com/133Seven/AlwaysAlpha_Launcher/master/client/install.exe", "install.exe")
            Catch ex As Exception
                MsgBox("Error while downloading client.")
                Me.Close()
            End Try
            Dim objProcess As System.Diagnostics.Process
            objProcess = New System.Diagnostics.Process()
            objProcess.StartInfo.FileName = "install.exe"
            objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            objProcess.Start()

            'Wait until the process passes back an exit code 
            objProcess.WaitForExit()

            'Free resources associated with this process
            objProcess.Close()
            Shell("launch.exe " & My.Settings.username)
            Me.Close()
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '' Note: call this from frm's Load event!
        Dim r As Rectangle
        If Parent IsNot Nothing Then
            r = Parent.RectangleToScreen(Parent.ClientRectangle)
        Else
            r = Screen.FromPoint(Me.Location).WorkingArea
        End If

        Dim x = r.Left + (r.Width - Me.Width) \ 2
        Dim y = r.Top + (r.Height - Me.Height) \ 2
        Me.Location = New Point(x, y)
        TextBox1.Text = My.Settings.username
        ProgressBar1.Visible = False
        Label2.Visible = False
    End Sub

End Class
